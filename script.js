const taskInput = document.getElementById('taskInput');
const addButton = document.getElementById('addButton');
const taskList = document.getElementById('taskList');

addButton.addEventListener('click', addTask);

function addTask() {
  const taskText = taskInput.value;
  if (taskText.trim() === '') return;

  const li = document.createElement('li');
  li.innerHTML = `
    <span>${taskText}</span>
    <button class="editButton">Editar</button>
    <button class="deleteButton">Eliminar</button>
  `;

  taskList.appendChild(li);
  taskInput.value = '';

  const editButton = li.querySelector('.editButton');
  const deleteButton = li.querySelector('.deleteButton');

  editButton.addEventListener('click', () => editTask(li));
  deleteButton.addEventListener('click', () => deleteTask(li));
}

function editTask(li) {
  const taskSpan = li.querySelector('span');
  const newText = prompt('Editar tarea:', taskSpan.textContent);
  if (newText !== null) {
    taskSpan.textContent = newText;
  }
}

function deleteTask(li) {
  taskList.removeChild(li);
}
